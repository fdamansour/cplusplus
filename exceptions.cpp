#include <iostream>
#include "exceptions.h"
using namespace std;

unsigned getBibliothecaires(int visiteurs) noexcept{
    return 1+visiteurs/25;
}
void exceptions() noexcept{
    cout << "Exceptions : " << endl;
    int visiteurs;
    cout << "visiteurs : " ;
    cin >> visiteurs ;

    unsigned tables = 14;

    try {
        if (visiteurs == 0)
            throw 0;
        if(visiteurs<0)
            throw out_of_range("Trop petit");
        unsigned tablesParVisiteur = tables /visiteurs;
        cout << "Tables par Visiteur : " << tablesParVisiteur << endl;


    } catch( int& codeErreur){
        cout << "Code d'erreur : " << codeErreur << endl;
          } catch( out_of_range& exc){
        cout << "Exception - hors limites - : " << exc.what() << endl;

    }catch (...){
        cout<< "Erreur inattendue " << endl;
    }

if(noexcept (getBibliothecaires((visiteurs)))){
            cout << "Bibliothécairs  : " << getBibliothecaires(visiteurs) << endl;
        }
}
