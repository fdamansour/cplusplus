#include <iostream>
#include <vector>

#include "stl.h"
using namespace std;

void collections(){

    cout << "Collections : " << endl;
    vector <string> titres { "Illiade", "Odyssee", "Le Retour"};
    titres.push_back("La vengeance");
    titres.insert( titres.begin(), "Best of Hom�re");
    titres[0] = "L'" + titres[0];
    cout << titres.size() << " titres" <<  endl;
    for (unsigned i=0; i<titres.size() ; i++)
        cout << titres[i] <<  endl;
    for (auto it=titres.cbegin(); it!=titres.cend() ; it++)
        cout << *it <<  endl;
    for (const string& s: titres)
        cout << s <<  endl;


    vector <string> titres2 ;

    for (unsigned i=0; i<titres.size() ; i+=2)
        titres2.push_back(titres[i]) ;

    cout << titres2.size() << " titres" <<  endl;
    for (unsigned i=0; i<titres2.size() ; i++)
        cout << titres2[i] <<  endl;
    //afficher tous les titres
    // cr�er un nouveau vecteur � partir des titres 0, 2, 4 ....


};
