#ifndef OBJETS_H_INCLUDED
#define OBJETS_H_INCLUDED

using namespace std;

void basesDeLobjet();
void objetsComplets();
void formeCanonique();

struct Employe {
    string nom;
    int id;

    Employe();
    Employe(string);
    Employe(string,int);
    ~Employe();
    void afficher();
};

// canonique :
// - ctor()
// - dtor()
// - ctor(&src) (recopie)
// - ctor(&&src) (placement)
// - op=(&src) (affectation)
struct Equipe {
    Employe * employes;
    unsigned taille;

    Equipe();
    Equipe(unsigned);
    Equipe(const Equipe&);
    Equipe(Equipe&&);
    ~Equipe();
    void afficher();
    Equipe& operator=(const Equipe&);
    Equipe& operator=(Equipe&&);
};

#endif // OBJETS_H_INCLUDED
