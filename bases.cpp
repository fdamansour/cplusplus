#include <iostream>
using namespace std;

void TypesEtTestsEtBoucles()
{
    cout << "Mediatheque de Toulouse :" << endl;
    // int ouvrages = 254000;
    // signed ouvrages = 254000;
    signed int ouvrages = 254'000; // C++11
    unsigned dvds = 2890;
    // int caisses = 0b1001; // C++11
    // int caisses = 011;
    // int caisses = 9;
    int caisse = 0x9;
    cout << "Taille d'un signed int : " << sizeof(ouvrages) << "o" << endl; // 4o
    char antennes = 8;
    short rayons = 460;
    int cds = 82300; //4o
    long emprunts = 123'200;
    long long entrees = 342'023;
    cout << "Tailles : " << sizeof(antennes) << "o " <<
        sizeof(rayons) << "o " <<
        sizeof(cds) << "o " <<
        sizeof(emprunts) << "o " <<
        sizeof(entrees) << "o " << endl; //  1-1o 2+o 2-4o 4-8o 8+o
        // Windows 64bits :  1o 2o 4o 4o 8o
        // Linux 64bits :  1o 2o 4o 8o 8o
    int16_t auteurs = 1219;
    int16_t max_auteurs = INT16_MAX;
    float emprunts_par_jour = 4211.8f;
    double emprunts_par_ans = 322123.1;
    double emprunts_par_ans2 = 3.221231e5;
    bool ouverte = true;
    string nom = "Mediateque rose";
    rayons = ((rayons * 100) % 43) >> 4;
    //bool assez_de_rayons = (rayons > 100) & (rayon < 500);
    bool assez_de_rayons = (rayons > 100) && (rayons < 500);
    bool cd_anormaux = (cds < 0) || (cds > 1'000'000);
    bool cd_normaux = !cd_anormaux;
    bool un_nombre_rond = (cds == 10'000) ^ (ouvrages == 100'000);
    // cds = 82300
    cds = cds + 1;
    cds += 1;
    ++cds;
    cds++;
    // int taille_bac = 870 / 10.999; // nombre de cd de 11mm par bac de 870mm
    double taille_bac_dbl = 870 / 10.999;
    // conversions :
    //int taille_bac = (int) taille_bac_dbl; // transtypage du C
    int taille_bac = int(taille_bac_dbl); // conversion du C++
    // + conversions de RTTI
    int bacs_cd_necessaires = (cds + taille_bac - 1) / taille_bac;
    cout << "Bacs de CD de 87cm  acheter en tout : " << bacs_cd_necessaires << endl; // 104Z
    int espace_restant = taille_bac * bacs_cd_necessaires - cds; // espace restant en nombre de cds
    cout << "Espace restant dans le dernier bac : " << espace_restant << " cds " << endl;
    if(espace_restant > 10) {
        cout << "On peut mettre aussi les 5 cassettes audios" << endl;
    } else
        cout << "On ne peut pas mettre les 5 cassettes audios" << endl;
    int camion_de_bac = bacs_cd_necessaires / 260;
    switch(bacs_cd_necessaires) {
    case 0:
        cout << "on dmnage tout en voiture" << endl;
        break;
    case 1:
    case 2:
        cout << "un seul camion voire 2" << endl;
        break;
    default:
        cout << "plusieurs camions" << endl;
    }
    // a partir du nombre de CDS, afficher l'un de ceux l :
    switch(cds/50'000) {
    case 0:
        cout << "Moins de 50 000 CDs" << endl;
        break;
    case 1:
    case 2:
        cout << "50 000  150 000 CDs" << endl;
        break;
    case 3:
    case 4:
    case 5:
        cout << "150 000  300 000 CDs" << endl;
        break;
    default:
        cout << "Plus de 300 000 CDs" << endl;
    }

    unsigned d = 0;
    while(d < bacs_cd_necessaires) {
        cout << "| Bac CD n" << d << "! |" << endl;
        d += 100;
    }

    d = 0;
    do {
        cout << "# Bac CD n" << d << "! #" << endl;
        d += 200;
    } while(d < bacs_cd_necessaires);

    for(d = 0 ; d < bacs_cd_necessaires ; d += 300) {
        cout << "[ Bac CD n" << d << "! ]" << endl;
    }

    // espace_restant = 14
    for(int i=0, j=0 ; i<espace_restant ; i+=j, j++) {
        cout << "Cassette : " << i << endl; // 0 0 1 3 6 10
    }

    float usagers_par_jour[7]; // ok
    //float usagers_par_jour[3+2*2]; // ok
    //int jours = 7;
    //float usagers_par_jour[jours]; // KO
    //const int JOURS = 7;
    //float usagers_par_jour[JOURS]; // ok
    //float usagers_par_jour[JOURS*2-7]; // ok
    usagers_par_jour[0] = 125.98f;
    // usagers_par_jour[7] = 90; // crash ?
    float usagers_weekend [] { 320.5f, 0 };
    float usagers_par_demi_jour[7][2]; // 56 octets
    usagers_par_demi_jour[6][1] = 0; // dimanche aprm

    for(float uj : usagers_par_jour) // C++11
        cout << uj << " ";

    // classification Dewey (1 : philosophie ... 9 : histoire)
    unsigned livres_par_rayon []{ 2300, 1432, 2394, 22333, 23209, 9893, 10923, 9288, 1420 };
    unsigned plus_petit_rayon = 0, plus_grand_rayon = 0;
    for(unsigned i = 1 ; i < 9 ; i++) {
        if(livres_par_rayon[i] < livres_par_rayon[plus_petit_rayon])
            plus_petit_rayon = i;
        if(livres_par_rayon[i] > livres_par_rayon[plus_grand_rayon])
            plus_grand_rayon = i;
    }

    cout << "Rayon avec le moins de livres : " << (plus_petit_rayon+1) << endl; // nombre entre 1 et 9
    cout << "Rayon avec le plus de livres : " << (plus_grand_rayon+1) << endl;

}
