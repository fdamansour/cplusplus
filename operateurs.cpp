#include <iostream>

#include "operateurs.h"

using namespace std;

// Code de la classe Rayon :
Rayon::Rayon(): Rayon("", 0) {}
Rayon::Rayon(const string n, const unsigned o):
    nom(n), ouvrages(o) {}
void Rayon::afficher() const {
    cout << nom << " (" << ouvrages << ")" << endl;
}

Rayon Rayon::operator+(const Rayon & src2)const {
    Rayon rt(this->nom + "/" + src2.nom,this->ouvrages + src2.ouvrages);
    return rt;
}

bool Rayon::operator==(const Rayon & src2)const {
    return (this->nom==src2.nom);
}

bool Rayon::operator!=(const Rayon & src2)const {
    return !(*this==src2);
}

void Rayon::operator()()const {
       afficher() ;
}

void visibiliteEtOperateurs() {
    cout << "Visibilit et operateurs" << endl;
    // test de la classe Rayon :
    Rayon r1 { "Geographie", 8711 };
    r1.ouvrages += 62; // ok : dans une amie de Rayon
    r1.afficher();

    Rayon r2 { "histoire", 9711 };

    Rayon r3 =r1+r2;

    r3.afficher();

    cout << "r1==r2? " << (r1==r2) << endl;
    cout << "r1!=r2? " << (r1!=r2) << endl;
    r2.nom=r1.nom;

    cout << "r1==r2? " << (r1==r2) << endl;
    cout << "r1!=r2? " << (r1!=r2) << endl;


    r1();
    Rayon r4 { "blabla", 8711 };
    r4.afficher();
    r4();
}


